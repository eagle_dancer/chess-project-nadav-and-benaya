
#include "Board.h"
#include "Game.h"
#include "King.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "Pawn.h"
#include <iostream>

/*
DTOR for Board object
Input: None
Output: None
*/

Board::~Board()
{
	for (int i = 0; i < BOARD_DIMENSION; i++)
	{
		delete[] this->_board[i];
	}
	delete[] this->_board;
}

/*
CTOR for Board object
INPUT: None
OUTPUT: None
*/

Board::Board()
{
	_board = new Spot*[BOARD_DIMENSION];
	for (int i = 0; i < BOARD_DIMENSION; i++)
	{
		_board[i] = new Spot[BOARD_DIMENSION];
	}
}

/*
Returns the Spot object at certain coordinates of _board feild
INPUT: x coord and y coord, int x2
OUTPUT: Spot object at location, Spot object
*/

Spot Board::getSpot(const int x, const int y) const
{
	return this->_board[y][x];
}

/*
Sets the peice of a spot object at certain coordinates of _board feild
INPUT: x coord and y coord, new peice, int x2, pointer to peice object
OUTPUT: None
*/

void Board::setSpotPeice(const int x, const int y, Spot spotWithPeice)
{
	this->_board[y][x].setPeice(spotWithPeice.getPeice());
	
}

void Board::setSpotEmpty(const int x, const int y)
{
	this->_board[y][x].setEmpty();
}



/*
Sets the _board feild to initial state
INPUT: None
OUTPUT: None
*/

void Board::resetBoard()
{

	Spot* newSpot;


	newSpot = new Spot(0, 0, new Rook(WHITE));
	this->_board[0][0] = *newSpot;

	newSpot = new Spot(1, 0, new Knight(WHITE));
	this->_board[0][1] = *newSpot;

	newSpot = new Spot(2, 0, new Bishop(WHITE));
	this->_board[0][2] = *newSpot;

	newSpot = new Spot(3, 0, new Queen(WHITE));
	this->_board[0][3] = *newSpot;

	newSpot = new Spot(4, 0, new King(WHITE));
	this->_board[0][4] = *newSpot;

	newSpot = new Spot(5, 0, new Bishop(WHITE));
	this->_board[0][5] = *newSpot;

	newSpot = new Spot(6, 0, new Knight(WHITE));
	this->_board[0][6] = *newSpot;

	newSpot = new Spot(7, 0, new Rook(WHITE));
	this->_board[0][7] = *newSpot;

	
	for (int j = 0; j < BOARD_DIMENSION; j++) {
		newSpot = new Spot(j, 1, new Pawn(WHITE));
		this->_board[1][j] = *newSpot;
	}
	

	newSpot = new Spot(0, 7, new Rook(BLACK));
	this->_board[7][0] = *newSpot;

	newSpot = new Spot(1, 7, new Knight(BLACK));
	this->_board[7][1] = *newSpot;

	newSpot = new Spot(2, 7, new Bishop(BLACK));
	this->_board[7][2] = *newSpot;

	newSpot = new Spot(3, 7, new Queen(BLACK));
	this->_board[7][3] = *newSpot;

	newSpot = new Spot(4, 7, new King(BLACK));
	this->_board[7][4] = *newSpot;

	newSpot = new Spot(5, 7, new Bishop(BLACK));
	this->_board[7][5] = *newSpot;

	newSpot = new Spot(6, 7, new Knight(BLACK));
	this->_board[7][6] = *newSpot;

	newSpot = new Spot(7, 7, new Rook(BLACK));
	this->_board[7][7] = *newSpot;


	for (int j = 0; j < BOARD_DIMENSION; j++) {
		newSpot = new Spot(j, 6, new Pawn(BLACK));
		this->_board[6][j] = *newSpot;
	}
	

	for (int i = 2; i < 6; i++) {
		for (int j = 0; j < BOARD_DIMENSION; j++) {
			this->_board[i][j] = *new Spot(j, i, NULL);
		}
	}

}

