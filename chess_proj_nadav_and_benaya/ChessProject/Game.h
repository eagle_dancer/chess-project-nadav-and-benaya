#pragma once
#include <string>
#include "Spot.h"
#include "Board.h"
#include "MoveStates.h"
#include "MoveState.h"

#define WHITE 0
#define BLACK 1
#define NONE -1
#define LETTER_ASCII_DIFF 97
#define NUM_ASCII_DIFF 48
#define FALSE 0
#define TRUE !FALSE
#define BOARD_DIMENSION 8

class Game
{

	public:

		Game();
		~Game();
		bool getPlayerTurn() const;
		void setPlayerTurn(const bool turn);
		void translateBoard(char* msgToFrontend);
		void translateMove(Spot& src, Spot& dest, const std::string move);
		void setBlackScore(const int score);
		void setWhiteScore(const int score);
		int getPlayerWon() const;
		void isChess(const bool playerInChess, const bool exception);
		void checkBasicExceptions(const Spot& src, const Spot& dest);
		void printBoard();
		Spot getWhiteKingPos() const;
		Spot getBlackKingPos() const;
		void updateKingPos(const Spot& pos);
			
		Board board;

	private:

		bool _playerTurn;
		int _blackScore;
		int _whiteScore;
		int _playerWon;
		Spot whiteKingPos;
		Spot blackKingPos;
};
