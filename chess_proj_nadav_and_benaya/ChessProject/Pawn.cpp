#include "Pawn.h"
#include "MoveStates.h"

/*
Pawn object CTOR
Input: Value for _team feild, int
Output: None
*/

Pawn::Pawn(const int _team) : Peice(_team)
{

	this->_type = this->_team ? 'p' : 'P';
	this->_hasMove = false;
}

/*
Getter for _hasMove feild of pawn object
Input: None
Output: current value of feild, bool
*/

bool Pawn::getHasMove() const
{
	return this->_hasMove;
}

/*
setter of _hasmove
Input: bool
Output: none
*/

void Pawn::setHasMove( bool has)
{
	this->_hasMove = has;
}

/*
	Function moves the pawn or throws exception if fails.

	Input:
			Board board - the board of the game.

			int destX --\
						--- the Dest spot.
			int destY --/

			int srcX --\
						--- the Source spot.
			int srcY --/

			bool chessCheck - the result of the chess check 

	Output:
			The move's legality. ( with 'throw' )
*/
void Pawn::move(const Board& board, int destX, int destY, int srcX, int srcY, bool chessCheck) 
{


	if (board.getSpot(destX, destY).getPeice() == nullptr)
	{

		if ((( std::abs(destY - srcY) == 2) && (srcX == destX)) && ((board.getSpot(srcX, srcY + (this->_team ? -1 : 1)).getPeice() == nullptr) && (!this->getHasMove())))

		{

			if (chessCheck)
			{
				throw moveState0();
			}
			else
			{
				setHasMove(true);
			}

		}
		else if (srcY + (this->_team ? -1 : 1) == destY && srcX == destX) //Movement 
		{
			if (chessCheck)
			{
				throw moveState0();
			}
			else
			{
				setHasMove(true);
			}
		}
		else
		{
			throw moveState6();
		}
	}
	else 
	{
		if (std::abs(srcX - destX) == 1 && srcY + (this->_team ? -1 : 1) == destY &&
			 board.getSpot(destX, destY).getPeice()->getTeam() != this->_team) //Capture
		{
			if (chessCheck)
			{
				throw moveState0();
			}
			else
			{
				setHasMove(true);
			}
		}
		else
		{
			throw moveState6();
		}
	}
	

}


/*
Empty override of virtual function
INPUT: The current board, the starting position to check, the end position to check, axis to check along, what axis we are checking along, Board object reference, int x3, bool
OUTPUT: None
*/

void Pawn::lookAround(const Board& board, int startPos, int endPos, int otherAxis, bool checkX) const
{
}

