#pragma once

#include <string>
#include "Spot.h"
#include "Board.h"



class Board;

class Peice
{
	public:

		Peice();
		Peice(bool _team);
		bool getTeam() const;
		char getType() const;
		virtual void move(const Board& board, int destX, int destY, int srcX, int srcY, bool chessCheck) = 0;
		virtual void lookAround(const Board& board, int startPos, int endPos, int otherAxis, bool checkX) const = 0;


protected:
		bool _team;
		char _type;

};
