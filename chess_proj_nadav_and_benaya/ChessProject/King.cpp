#include "King.h"
#include "MoveStates.h"

/*
King object CTOR
Input: value for _team feild, int
Output: None
*/

King::King(const int _team) : Peice(_team)
{

	this->_type = this->_team ? 'k' : 'K';
	
}

/*
	Function moves the King or throws exception if fails.

	Input:
			Board board - the board of the game.

			int destX --\
						--- the Dest spot.
			int destY --/

			int srcX --\
						--- the Source spot.
			int srcY --/

			bool chessCheck - the result of the chess check 

	Output:
			The move's legality. ( with 'throw' )
*/

void King::move(const Board& board, int destX, int destY, int srcX, int srcY, bool chessCheck) 
{
	if (std::abs(srcX - destX) > 1 || std::abs(srcY - destY) > 1)
	{
		throw moveState6();
	}

	else if (board.getSpot(destX, destY).getPeice() && // chack self-cupcher
		board.getSpot(destX, destY).getPeice()->getTeam() == this->_team)
	{

		throw moveState3();
	}

	
	if (chessCheck)
	{
		throw moveState0();
	}
}

/*
Empty override of virtual function
INPUT: The current board, the starting position to check, the end position to check, axis to check along, what axis we are checking along, Board object reference, int x3, bool
OUTPUT: None
*/

void King::lookAround(const Board& board, int startPos, int endPos, int otherAxis, bool checkX) const
{
}
