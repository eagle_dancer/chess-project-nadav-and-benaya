#include "Bishop.h"
#include "Rook.h"
#include "MoveStates.h"
#include "Game.h"

/*
Bishop object standard CTOR
Input: value for _team feild, int
Output: None
*/

Bishop::Bishop() : Peice()
{

}

/*
Bishop object CTOR
Input: value for _team feild, int
Output: None
*/

Bishop::Bishop(const int _team) : Peice(_team)
{
	this->_type = this->_team ? 'b': 'B';

}
 
/*
Empty override of virtual function
INPUT: The current board, the starting position to check, the end position to check, axis to check along, what axis we are checking along, Board object reference, int x3, bool
OUTPUT: None
*/

void Bishop::lookAround(const Board& board, int startPos, int endPos, int otherAxis, bool checkX) const
{
	/*
		we wanted to use lookAround for bishop aswell but it was far too hard, most of the while loops do different things
		so not exactly D.R.Y. but still..
	*/
}



/*
	Method checks if the bishop move is legal.
	Function moves the Bishop or throws exception if fails.

	Input:
			Board board - the board of the game.

	Output:
			The move's legality.
*/

		
void Bishop::move(const Board& board, int destX, int destY, int srcX, int srcY, bool chessCheck) 
{
	bool isXbigger = srcX > destX;
	bool isYbigger = srcY > destY;

	if (isXbigger && isYbigger)
	{
		unsigned int i = srcX - 1, j = srcY - 1;
		while (i > destX && j > destY)
		{
			if (board.getSpot(i, j).getPeice() != nullptr)
			{
				throw moveState6();
			}

			i--;
			j--;
		}

		if (i != destX || j != destY)
		{
			throw moveState6();
		}

		

	}

	else if (isXbigger && !isYbigger)
	{
		unsigned int i = srcX - 1, j = srcY + 1;
		while (i > destX && j < destY)
		{
			if (board.getSpot(i, j).getPeice() != nullptr)
			{
				throw moveState6();
			}

			i--;
			j++;
		}

		if (i != destX || j != destY)
		{
			throw moveState6();
		}

		
	}

	else if (!isXbigger && isYbigger)
	{
		unsigned int i = srcX + 1, j = srcY - 1;
		while (i < destX && j > destY)
		{
			if (board.getSpot(i, j).getPeice() != nullptr)
			{
				throw moveState6();
			}

			i++;
			j--;
		}

		if (i != destX || j != destY)
		{
			throw moveState6();
		}

		

	}

	else
	{
		unsigned int i = srcX + 1, j = srcY + 1;
		while (i < destX && j < destY)
		{
			if (board.getSpot(i, j).getPeice() != nullptr)
			{
				throw moveState6();
			}

			i++;
			j++;
		}

		if (i != destX || j != destY)
		{
			throw moveState6();
		}

		

	}

	if (board.getSpot(destX, destY).getPeice() != nullptr &&
		board.getSpot(destX, destY).getPeice()->getTeam() == this->_team) // chack self-cupcher
	{
		throw moveState3();
	}

	if (chessCheck)
	{
		throw moveState0();
	}

}
