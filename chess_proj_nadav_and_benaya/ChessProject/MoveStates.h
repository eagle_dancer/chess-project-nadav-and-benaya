#pragma once

#include "MoveState.h"

/*
Exactly like what is shown on slide 10 of the presentation explaining the chess project
State 0: Valid move
State 1: Valid move and checkmate!
States 2 - 7: Exceptions
*/

class moveState0 : public MoveState
{

	public:

		moveState0();
		virtual std::string message() const override;
};

class moveState1 : public MoveState
{
	public:

		moveState1();
		virtual std::string message() const override;
};


class moveState2 : public MoveState
{
	public:

		moveState2();
		virtual std::string message() const override;
};

class moveState3 : public MoveState
{
	public:

		moveState3();
		virtual std::string message() const override;
};

class moveState4 : public MoveState
{
	public:

		moveState4();
		virtual std::string message() const override;
};

class moveState5 : public MoveState
{
	public:

		moveState5();
		virtual std::string message() const override;
};

class moveState6 : public MoveState
{
	public:

		moveState6();
		virtual std::string message() const override;
};

class moveState7 : public MoveState
{
	public:

		moveState7();
		virtual std::string message() const override;
};
