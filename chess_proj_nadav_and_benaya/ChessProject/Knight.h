#pragma once
#include "Peice.h"


class Knight : public Peice
{
public:
	Knight();
	Knight(const int _team);
	virtual void move(const Board& board, int destX, int destY, int srcX, int srcY, bool chessCheck) override; // movment function
	virtual void lookAround(const Board& board, int startPos, int endPos, int otherAxis, bool checkX) const override;
};

