#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"
#include "Game.h"
//#include <string>

/*
Queen object standard CTOR
Input: value for _team feild, int
Output: None
*/

Queen::Queen(const int _team) : Peice(_team)
{
	this->_type = this->_team ? 'q' : 'Q';
}


/*
	Function moves the Queen or throws exception if fails.

	Input:
			Board board - the board of the game.

			int destX --\
						--- the Dest spot.
			int destY --/

			int srcX --\
						--- the Source spot.
			int srcY --/

			bool chessCheck - the result of the chess check 

	Output:
			The move's legality. ( with 'throw' )
*/

void Queen::move(const Board& board, int destX, int destY, int srcX, int srcY, bool chessCheck) 
{

	try
	{
		Rook().move(board, destX, destY, srcX, srcY, FALSE);
		throw moveState0();
	}
	catch (const MoveState& e)
	{
		
		if (atoi(e.getState()) != 0)
		{
			try
			{
				Bishop().move(board, destX, destY, srcX, srcY, FALSE);
				throw moveState0();

			}
			catch (const MoveState& e)
			{
				if (atoi(e.getState()) != 0)
				{
					throw moveState6();
				}
				else if (chessCheck)
				{
					throw moveState0();
				}

			}
		}
		else if (atoi(e.getState()) == 0 && chessCheck)
		{
			throw moveState0();
		}
		
	}

}

/*
Empty override of virtual function
INPUT: The current board, the starting position to check, the end position to check, axis to check along, what axis we are checking along, Board object reference, int x3, bool
OUTPUT: None
*/

void Queen::lookAround(const Board& board, int startPos, int endPos, int otherAxis, bool checkX) const
{
}
