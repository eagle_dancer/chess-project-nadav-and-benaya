#pragma once

#include "Peice.h"

class Peice;
class Spot
{
	public:


		/*
		Here I return Peice as a pointer to a peice object because it is an abstract class that others are inheriting from
		*/

		
		Spot(int _x, int _y, Peice* peice);
		Peice* getPeice() const;
		void setPeice(Peice* newPeice);
		Spot();
		int getX() const;
		int getY() const;
		void setX(const int _x);
		void setY(const int _y);
		void setEmpty();



	private:

		Peice* _peice;
		int _x;
		int _y;

};
