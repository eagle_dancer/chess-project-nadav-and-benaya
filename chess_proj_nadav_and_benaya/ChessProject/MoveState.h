#pragma once

#include <string>

class MoveState
{

	public:

		MoveState();
		~MoveState();
		char* getState() const;
		virtual std::string message() const = 0;

	protected:

		char* _state;


};
