#include "Game.h"
#include "King.h"
#include "MoveStates.h"
#include <iostream>
#include <vector>
#define _CRT_SECURE_NO_WARNINGS

/*
Game class CTOR
Input: None
Output: None
*/

Game::Game()
{
    this->_playerTurn = WHITE;
    this->_blackScore = 0;
    this->_whiteScore = 0;
    this->_playerWon = NONE;
    this->blackKingPos = Spot(4, 7, NULL);
    this->whiteKingPos = Spot(4, 0, NULL);
    this->board.resetBoard();

}

/*
Game class DTOR
Input: None
Output: None
*/

Game::~Game()
{

    for (int i = 0; i < BOARD_DIMENSION; i++)
    {
        for (int j = 0; j < BOARD_DIMENSION; j++)
        {
            if (this->board.getSpot(i, j).getPeice())
            {
                delete this->board.getSpot(i, j).getPeice();
            }
        }
    }

}

/*
Returns _playerTurn feild of Game object
Input: None
Output: value of _playerTurn, bool
*/

bool Game::getPlayerTurn() const
{
    return this->_playerTurn;
}

/*
Returns _playerTurn feild of Game object
Input: None
Output: value of _playerTurn, bool
*/

void Game::setPlayerTurn(const bool turn)
{
    this->_playerTurn = turn;
}

/*
Translates the current board state into frontend protocol
Input: None
Output: The frontend protocol, pointer to char
*/

void Game::translateBoard(char* msgToFrontend)
{
    std::string boardCode = "";
    Peice* currPeice;
    int row = 0;
    int col = 0;

    for (row = 0; row < BOARD_DIMENSION; row++)
    {
        for (col = 0; col < BOARD_DIMENSION; col++)
        {
            currPeice = this->board.getSpot(col, row).getPeice();


            if (!currPeice)
            {
                boardCode += '#';
            }
            else
            {
                boardCode += currPeice->getType();
            }
        }
    }

    // Starting player

    boardCode += std::to_string(WHITE);

    strcpy(msgToFrontend, boardCode.c_str());

    msgToFrontend[65] = 0;

   
}

/*
Translates frontend move according to protocol
Input: source spot, destination spot, move message from frontend, Spot object reference x2, string
Output: None
*/

void Game::translateMove(Spot& src, Spot& dest, const std::string move)
{
    
    src.setX(((int) move[0]) - LETTER_ASCII_DIFF);
    src.setY(BOARD_DIMENSION - ((int) move[1] - NUM_ASCII_DIFF));

    dest.setX(((int)move[2]) - LETTER_ASCII_DIFF);
    dest.setY(BOARD_DIMENSION - ((int)move[3] - NUM_ASCII_DIFF));

    //src.setX(this->board.getSpot(srcX, srcY).getX());
    //src.setY(this->board.getSpot(srcX, srcY).getY());

    //dest.setX(this->board.getSpot(destX, destY).getX());
    //dest.setY(this->board.getSpot(destX, destY).getY());

    //src.setPeice(this->board.getSpot(src.getX(), src.getY()).getPeice());
    //dest.setPeice(this->board.getSpot(dest.getX(), dest.getY()).getPeice());
}

/*
Setter for _blackScore feild of Game object
INPUT: New value for feild, int
OUTPUT: None
*/

void Game::setBlackScore(const int score)
{
    this->_blackScore = score;
}

/*
Setter for _whiteScore feild of Game object
INPUT: New value for feild, int
OUTPUT: None
*/

void Game::setWhiteScore(const int score)
{
    this->_whiteScore = score;
}

/*
Getter for _playerWon feild of Game object
INPUT: None
OUTPUT: current valie, int
*/

int Game::getPlayerWon() const
{
    return this->_playerWon;
}

/*
Checks if an enemy peice is threatening the current players king (is in a chess) or if the move puts the current players king in a chess
INPUT: Which players king to check, If to notify for an exception or checkmate, bool, bool
OUTPUT: None
*/

void Game::isChess(const bool playerInChess, const bool exception)
{
    Spot kingPos;
    Peice* currPeice;
    int row = 0;
    int col = 0;
    //std::vector<Peice*> enemyPeices;

    if (playerInChess)
    {
        kingPos = this->getBlackKingPos();
    }
    else
    {
        kingPos = this->getWhiteKingPos();
    }

    for (row = 0; row < BOARD_DIMENSION; row++)
    {

        for (col = 0; col < BOARD_DIMENSION; col++)
        {

            currPeice = this->board.getSpot(col, row).getPeice();

            if (currPeice)
            {
                if (currPeice->getTeam() == !playerInChess)
                {
                    //enemyPeices.push_back(currPeice);

                    try
                    {
                        currPeice->move(this->board, kingPos.getX(), kingPos.getY(), col, row, TRUE);
                    }
                    catch (const MoveState& e)
                    {
                        if (atoi(e.getState()) == 0)
                        {
                            if (!exception)
                            {
                                throw moveState1();
                            }
                            else
                            {
                                throw moveState4();
                            }

                        }
                    }
                }
            }
        }
    }

    if (!exception)
    {
        throw moveState0();
    }

}

/*
Checks for basic exceptions of a move
INPUT: The source spot and destination spot, reference to Spot object x2
OUTPUT: None
*/

void Game::checkBasicExceptions(const Spot& src, const Spot& dest)
{
    if (src.getX() > BOARD_DIMENSION || src.getX() < 0 || src.getY() > BOARD_DIMENSION || src.getY() < 0 || dest.getX() > BOARD_DIMENSION || dest.getX() < 0 || dest.getY() > BOARD_DIMENSION || dest.getY() < 0)
    {
        throw moveState5();
    }
    else if (src.getX() == dest.getX() && src.getY() == dest.getY())
    {
        throw moveState7();
    }
    else if (!board.getSpot(src.getX(), src.getY()).getPeice() || board.getSpot(src.getX(), src.getY()).getPeice()->getTeam() != this->getPlayerTurn())
    {
        throw moveState2();
    }
    else if (board.getSpot(dest.getX(), dest.getY()).getPeice() && board.getSpot(dest.getX(), dest.getY()).getPeice()->getTeam() == this->getPlayerTurn())
    {
        throw moveState3();
    }

    
}

/*
Prints the current board
INPUT: None
OUTPUT: None
*/

void Game::printBoard()
{
    Peice* currPeice;

    for (int i = 0; i < BOARD_DIMENSION; i++)
    {
        std::cout << "|";

        for (int j = 0; j < BOARD_DIMENSION; j++)
        {
            currPeice = this->board.getSpot(j, i).getPeice();

            if (!currPeice)
            {
                std::cout << "#";
            }
            else
            {
                std::cout << currPeice->getType();
            }

            std::cout << "|";
        }

        std::cout << "\n";
    }

}

/*
Updates the king position feilds in the Game object
INPUT: The new position of a king, reference to Spot object
OUTPUT: None
*/

void Game::updateKingPos(const Spot& pos)
{
    if (pos.getPeice()->getType() == 'K')
    {
        this->whiteKingPos = pos;
    }
    else if (pos.getPeice()->getType() == 'k')
    {
        this->blackKingPos = pos;
    }
}

/*
Getter for whiteKingPos feild of Game object
INPUT: None
OUTPUT: value of feild, Spot object
*/

Spot Game::getWhiteKingPos() const
{
    return this->whiteKingPos;
}

/*
Getter for blackKingPos feild of Game object
INPUT: None
OUTPUT: value of feild, Spot object
*/

Spot Game::getBlackKingPos() const
{
    return this->blackKingPos;
}


