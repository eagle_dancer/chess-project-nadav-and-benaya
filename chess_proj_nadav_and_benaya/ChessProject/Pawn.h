#pragma once
#include "Peice.h"
#include "Spot.h"
#include "Board.h"


class Pawn : public Peice
{
public:
	Pawn();
	Pawn(const int _team);
	void move(const Board& board, int destX, int destY, int srcX, int srcY, bool chessCheck) override; // movment function
	virtual void lookAround(const Board& board, int startPos, int endPos, int otherAxis, bool checkX) const override;
	bool getHasMove() const;
	void setHasMove( bool has);
	
private:


	bool _hasMove;

};


