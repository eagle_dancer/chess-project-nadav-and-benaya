#include "MoveState.h"
#include "MoveStates.h"
#include <iostream>

/*
CTOR for abstract class MoveState, initializes feild _state
INPUT: None
OUTPUT: None
*/

MoveState::MoveState()
{
	this->_state = new char[2];
}

/*
DTOR for abstract class MoveState, clears _state feild
INPUT: None
OUTPUT: None
*/

MoveState::~MoveState()
{
	delete[] this->_state;
}

/*
Getter for _state feild from MoveState object
INPUT: None
OUTPUT: Value of _state feild, char*
*/

char* MoveState::getState() const
{
	return this->_state;
}

/*
CTOR for class MoveState0, sets _state feild to 0
INPUT: None
OUTPUT: None
*/

moveState0::moveState0()
{

	this->_state[0] = '0';
	this->_state[1] = 0;
	
}

/*
Redefinition of virtual message function for moveState0
INPUT: None
OUTPUT: message, string
*/

std::string moveState0::message() const
{
	return "Valid move\n";
}

/*
CTOR for class MoveState1, sets _state feild to 0
INPUT: None
OUTPUT: None
*/

moveState1::moveState1()
{
	this->_state[0] = '1';
	this->_state[1] = 0;
}

/*
Redefinition of virtual message function for moveState1
INPUT: None
OUTPUT: message, string
*/

std::string moveState1::message() const
{
	return "Valid move and checkmate!\n";
}

/*
CTOR for class MoveState2, sets _state feild to 0
INPUT: None
OUTPUT: None
*/

moveState2::moveState2()
{
	this->_state[0] = '2';
	this->_state[1] = 0;
}

/*
Redefinition of virtual message function for moveState2
INPUT: None
OUTPUT: message, string
*/

std::string moveState2::message() const
{
	return "Invalid move, source spot does not have current player peice\n";
}

/*
CTOR for class MoveState3, sets _state feild to 0
INPUT: None
OUTPUT: None
*/

moveState3::moveState3()
{
	this->_state[0] = '3';
	this->_state[1] = 0;
}

/*
Redefinition of virtual message function for moveState3
INPUT: None
OUTPUT: message, string
*/

std::string moveState3::message() const
{
	return "Invalid move, destination spot has current player peice\n";
}

/*
CTOR for class MoveState4, sets _state feild to 0
INPUT: None
OUTPUT: None
*/

moveState4::moveState4()
{
	this->_state[0] = '4';
	this->_state[1] = 0;
}

/*
Redefinition of virtual message function for moveState4
INPUT: None
OUTPUT: message, string
*/

std::string moveState4::message() const
{
	return "Invalid move, move will cause checkmate on current player\n";
}

/*
CTOR for class MoveState5, sets _state feild to 0
INPUT: None
OUTPUT: None
*/

moveState5::moveState5()
{
	this->_state[0] = '5';
	this->_state[1] = 0;
}

/*
Redefinition of virtual message function for moveState5
INPUT: None
OUTPUT: message, string
*/

std::string moveState5::message() const
{
	return "Invalid move, spot indexes do not exist\n";
}

/*
CTOR for class MoveState6, sets _state feild to 0
INPUT: None
OUTPUT: None
*/

moveState6::moveState6()
{
	this->_state[0] = '6';
	this->_state[1] = 0;
}

/*
Redefinition of virtual message function for moveState6
INPUT: None
OUTPUT: message, string
*/

std::string moveState6::message() const
{
	return "Invalid move, illegal movement of peice type\n";
}

/*
CTOR for class MoveState7, sets _state feild to 0
INPUT: None
OUTPUT: None
*/

moveState7::moveState7()
{
	this->_state[0] = '7';
	this->_state[1] = 0;
}

/*
Redefinition of virtual message function for moveState7
INPUT: None
OUTPUT: message, string
*/

std::string moveState7::message() const
{
	return "Invalid move, destination and source spots are the same\n";
}
