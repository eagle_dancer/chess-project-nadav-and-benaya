#pragma once

#include "Spot.h"
#include <iostream>

class Spot;
class Board
{
	public:

		/*
		Here I return spots as objects and not as pointers because i dont want them to be changed directly, I want them to obly
		be changed through setters
		*/

		~Board();
		Board();
		Spot getSpot(const int x, const int y) const;
		void setSpotPeice(const int x, const int y, Spot spotWithPeice);
		void setSpotEmpty(const int x, const int y);
		void resetBoard();

		


	private:

		Spot** _board;

};

