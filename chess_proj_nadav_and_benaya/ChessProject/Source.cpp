/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include "Game.h"
#include <stdlib.h>
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;

#define MOVE_STATE_4 4


void main()
{
	srand(time_t(NULL));

	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	
	Game game;
	Spot src(0, 0, NULL);	
	Spot dest(0, 0, NULL);
	Spot prevDest(0, 0, NULL);
	char msgToGraphics[1024];
	string msgFromGraphics;

	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE


	game.translateBoard(msgToGraphics);
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		
		game.translateMove(src, dest, msgFromGraphics);

		try
		{
			game.checkBasicExceptions(src, dest);

			game.board.getSpot(src.getX(), src.getY()).getPeice()->move(game.board, dest.getX(), dest.getY(), src.getX(), src.getY(), FALSE);

			prevDest = game.board.getSpot(dest.getX(), dest.getY());
			game.board.setSpotPeice(dest.getX(), dest.getY(), game.board.getSpot(src.getX(), src.getY()));
			game.board.setSpotEmpty(src.getX(), src.getY());

			game.updateKingPos(game.board.getSpot(dest.getX(), dest.getY()));

			game.isChess(game.getPlayerTurn(), TRUE);

			game.isChess(!game.getPlayerTurn(), FALSE);

		}
		catch (const MoveState& e)
		{

			std::cout << e.message();

			if (atoi(e.getState()) == MOVE_STATE_4)
			{

				game.board.setSpotPeice(src.getX(), src.getY(), game.board.getSpot(dest.getX(), dest.getY()));
				game.board.setSpotPeice(dest.getX(), dest.getY(), prevDest);

				game.updateKingPos(game.board.getSpot(src.getX(), src.getY()));



			}
			else if(atoi(e.getState()) < 2)
			{
				game.setPlayerTurn(!game.getPlayerTurn());
			}

			p.sendMessageToGraphics(e.getState());


		}
		 
		msgFromGraphics = p.getMessageFromGraphics();

		//game.translateMove(src, dest, msgFromGraphics);


		//strcpy_s(msgToGraphics, "YOUR CODE"); // msgToGraphics should contain the result of the operation

		/******* JUST FOR EREZ DEBUGGING ******/
		//int r = rand() % 10; // just for debugging......
		//msgToGraphics[0] = (char)(1 + '0');
		//msgToGraphics[1] = 0;
		/******* JUST FOR EREZ DEBUGGING ******/


		// return result to graphics		
		//p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		
	}

	p.close();
}