#include "Knight.h"
#include "MoveStates.h"
#include <math.h>

/*
Knight object CTOR
Input: value for _team feild, int
Output: None
*/

Knight::Knight(const int _team) : Peice(_team)
{

	this->_type = this->_team ? 'n' : 'N';
}



/*
	Function moves the Knight or throws exception if fails.

	Input:
			Board board - the board of the game.

			int destX --\
						--- the Dest spot.
			int destY --/

			int srcX --\
						--- the Source spot.
			int srcY --/

			bool chessCheck - the result of the chess check 

	Output:
			The move's legality. ( with 'throw' )
*/
void Knight::move(const Board& board, int destX, int destY, int srcX, int srcY, bool chessCheck) 
{
	if ( ! ( ( std::abs(srcX - destX) == 2 && std::abs(srcY - destY) == 1 ) ||
		( std::abs(srcY - destY) == 2 && std::abs(srcX - destX) == 1) ) ) 
	{
		throw moveState6();
	}
	else if(board.getSpot(destX, destY).getPeice()  && 
		board.getSpot(destX, destY).getPeice()->getTeam() == this->_team) // chack self-cupcher
	{
		throw moveState3();
	}
	else
	{
		if (chessCheck)
		{
			throw moveState0();
		}
	}
}

/*
Empty override of virtual function
INPUT: The current board, the starting position to check, the end position to check, axis to check along, what axis we are checking along, Board object reference, int x3, bool
OUTPUT: None
*/

void Knight::lookAround(const Board& board, int startPos, int endPos, int otherAxis, bool checkX) const
{
}
