#pragma once
#include "Peice.h"
#include "Rook.h"
#include "Bishop.h"
#include "MoveStates.h"

class Queen : public Peice
{
public:
	Queen();
	Queen(const int _team);
	void move(const Board& board, int destX, int destY, int srcX, int srcY, bool chessCheck) override; // movment function
	virtual void lookAround(const Board& board, int startPos, int endPos, int otherAxis, bool checkX) const override;
};

