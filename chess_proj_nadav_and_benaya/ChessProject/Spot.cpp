#include "Spot.h"

/*
CTOR for Spot object
Input: values of _x and _y feilds, int x2
Output: None
*/

Spot::Spot( int _x, int _y, Peice* peice)
{
	this->_peice = peice;
	this->_x = _x;
	this->_y = _y;
}

/*
Getter for _peice feild of Spot object
Input: None
Output: current value of feild, pointer to Peice object
*/

Peice* Spot::getPeice() const
{
	return this->_peice;
}

/*
Setter for _peice feild of Spot object
Input: New value for _peice feild, pointer to peice object
Output: None
*/

void Spot::setPeice(Peice* newPeice)
{
	this->_peice = newPeice;
}

/*
Standard CTOR for Spot object
Input: None
Output: None
*/

Spot::Spot()
{
}

/*
Getter for _x feild of Spot object
Input: None
Output: current value of feild, int
*/

int Spot::getX() const
{
	return this->_x;
}

/*
Getter for _y feild of Spot object
Input: None
Output: current value of feild, int
*/

int Spot::getY() const
{
	return this->_y;
}

/*
Setter for _x feild of Spot object
Input: New value of _x feild, int
Output: None
*/

void Spot::setX(const int _x)
{
	this->_x = _x;
}

/*
Setter for _y feild of Spot object
Input: New value of _y feild, int
Output: None
*/

void Spot::setY(const int _y)
{
	this->_y = _y;
}

/*
Sets _peice feild to NULL
Input: None
Output: None
*/

void Spot::setEmpty()
{
	this->_peice = NULL;
}
