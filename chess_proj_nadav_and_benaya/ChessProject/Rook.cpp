#include "Rook.h"
#include "Game.h"
#include "MoveStates.h"

/*
Rook object standard CTOR
Input: value for _team feild, int
Output: None
*/

Rook::Rook() : Peice()
{
}

/*
Rook object CTOR
Input: value for _team feild, int
Output: None
*/

Rook::Rook(const int _team) : Peice(_team)
{
	
	this->_type = this->_team ? 'r' : 'R';
}

/*
Checks spaces before destination for peices blocking the way
INPUT: The current board, the starting position to check, the end position to check, axis to check along, what axis we are checking along, Board object reference, int x3, bool
OUTPUT: None
*/


void Rook::lookAround(const Board& board, int startPos, int endPos, int otherAxis, bool checkX) const
{
	for (unsigned int i = startPos; i < endPos; i++)
	{
		if (!checkX)
		{
			if (board.getSpot(otherAxis, i).getPeice() != nullptr)
			{
				throw moveState6();
			}
		}
		else
		{
			if (board.getSpot(i, otherAxis).getPeice() != nullptr)
			{
				throw moveState6();
			}
		}
		
	}
}

/*
	Function moves the Rook or throws exception if fails.

	Input:
			Board board - the board of the game.

			int destX --\
						--- the Dest spot.
			int destY --/

			int srcX --\
						--- the Source spot.
			int srcY --/

			bool chessCheck - the result of the chess check

	Output:
			The move's legality. ( with 'throw' )
*/

void Rook::move(const Board& board, int destX, int destY, int srcX, int srcY, bool chessCheck) 
{
	bool sameX = srcX == destX;
	bool sameY = srcY == destY;



	// the next 4 blokes check if there is a pice on our rook way. //
	if (sameX && !sameY)
	{
		if (srcY > destY)
		{
			
			this->lookAround(board, destY+1, srcY, srcX, FALSE);
		}

		else
		{
			
			this->lookAround(board, srcY+1, destY, srcX, FALSE);
		}
	}

	else if (!sameX && sameY)
	{
		if (srcX > destX)
		{
			
			this->lookAround(board, destX+1, srcX, srcY, TRUE);
		}

		else
		{
			
			this->lookAround(board, srcX+1, destX, srcY, TRUE);
		}
	}

	else if(!sameX && !sameY)
	{
		throw moveState6();
	}

	if (board.getSpot(destX, destY).getPeice() != nullptr &&
		board.getSpot(destX, destY).getPeice()->getTeam() == this->_team) // chack self-cupcher
	{
		throw moveState3();
	}

	if (chessCheck)
	{
		throw moveState0();
	}
	
}
