#pragma once

#include "Peice.h"
#include "Spot.h"
#include "Board.h"


class King : public Peice
{
public:
	King();
	King(const int _team);
	virtual void move(const Board& board, int destX, int destY, int srcX, int srcY, bool chessCheck)  override; // movment function
	virtual void lookAround(const Board& board, int startPos, int endPos, int otherAxis, bool checkX) const override;

	
};
