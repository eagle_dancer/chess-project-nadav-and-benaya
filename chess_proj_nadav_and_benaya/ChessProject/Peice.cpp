#include "Peice.h"

/*
Peice object standard CTOR
Input: None
Output: None
*/

Peice::Peice()
{
}

/*
Peice object standard CTOR
Input: Value of _team feild
Output: None
*/

Peice::Peice(bool _team)
{
	this->_team = _team;
}

/*
Getter for _team feild of Peice object
Input: None
Output: current value of feild, bool
*/

bool Peice::getTeam() const
{
	return this->_team;
}

/*
Getter for _type feild of Peice object
Input: None
Output: current value of feild, string
*/

char Peice::getType() const
{
	return this->_type;
}



