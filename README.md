Our awesome chess project !

Added _CRT_SECURE_NO_WARNINGS to preprocessor definitions in project properties to avoid deprecation errors
as explained here: https://stackoverflow.com/questions/22450423/how-to-use-crt-secure-no-warnings/22450838
